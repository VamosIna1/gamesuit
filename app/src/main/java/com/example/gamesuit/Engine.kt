package com.example.gamesuit

import android.os.CountDownTimer
import java.util.*
class GameEngine {

    val TIME_UNIT: Long = 21000
    var timer:CountDownTimer? = null

    fun init(state: GameState){
        state.onStartEngine()
        timer = object : CountDownTimer(TIME_UNIT, 4000){

            override fun onFinish() {
                state.onFinish()
            }

            override fun onTick(p0: Long) {
                state.onUpdate(p0)
            }
        }
    }

    fun start(){
        timer?.start()
    }

    fun stop(){
        timer?.cancel()
    }


}