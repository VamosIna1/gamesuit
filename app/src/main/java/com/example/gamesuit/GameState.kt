package com.example.gamesuit

interface GameState {



    fun onStartEngine()
    fun onUpdate(time:Long)
    fun onFinish()
}