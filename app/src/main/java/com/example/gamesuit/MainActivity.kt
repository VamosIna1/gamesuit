package com.example.gamesuit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import java.util.*
import android.view.LayoutInflater
import android.widget.*
import androidx.core.view.ViewCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), GameState, View.OnClickListener {


    val gameEngine:GameEngine = GameEngine()
    var score:Int = 0
    var time:Int = 900
    var dialog:AlertDialog? = null
    var curType: type? = null
    var curState: state? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        setContentView(R.layout.layout_dialog)

        gameEngine.init(this)
        tv_info.text = "Siap siap"
        tv_score.text = "Score : $score"
        tv_time.text = "Time : $time"
        generateDialog()

        tv_time.postDelayed({
            gameEngine.start()
            tv_info.text = ""

        }, 1000)

    }

    override fun onStartEngine() {
        btn_paper.setOnClickListener(this)
        btn_rock.setOnClickListener(this)
        btn_scissor.setOnClickListener(this)
        randomEnemy()
        curState = state.ongame
    }

    override fun onUpdate(tm: Long) {
        if(curState == state.ongame) {
            time--
            tv_time.text = "Time : $time"
        }

        if(time == 0){
            dialog!!.setMessage("Score : $score")
            dialog!!.show()
            curState = state.finish
            gameEngine.stop()
            ViewCompat.animate(img_enemy).translationY(-400f).duration = 1000
        }
    }

    override fun onFinish() {

    }

    fun generateDialog(){
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        val view:View = LayoutInflater.from(this).inflate(R.layout.layout_dialog, null, false)
        builder.setView(view)
        builder.setTitle("Result")
        builder.setMessage("Score akhir : $score")
        builder.setPositiveButton("restart") { dialog, which ->
            dialog.dismiss()
            restart()
        }
        builder.setCancelable(false)
        dialog = builder.create()
    }

    enum class type{
        rock,
        paper,
        scissor
    }

    enum class state{
        ongame,
        finish,
    }

    override fun onClick(v: View) {
        if(curState == state.finish)
            return

        when(v.id){
            R.id.btn_rock->{
                img_mc.setImageResource(R.drawable.ic_rock)
                cekAnswer(type.rock)
            }
            R.id.btn_paper->{
                img_mc.setImageResource(R.drawable.ic_paper)
                cekAnswer(type.paper)
            }
            R.id.btn_scissor->{
                img_mc.setImageResource(R.drawable.ic_scissor)
                cekAnswer(type.scissor)
            }
        }
        ViewCompat.animate(img_mc).translationY(-600f).duration = 100

    }


    fun cekAnswer(tp: type){
        var info:String = ""
        if(curType == type.rock){
            if(tp == type.rock){
                info = "draw" //equal
            }else if(tp == type.paper){
                info = "MENANG" //win
                score+=10
            }else{
                info = "KALAH" //lose
            }
        }else if(curType == type.paper){
            if(tp == type.rock){
                info = "KALAH" // lose
            }else if(tp == type.paper){
                info = "DRAW" //equal
            }else{
                info = "MENANG" //win
                score+=10
            }
        }else{
            if(tp == type.rock){
                info = "MENANG" // win
                score+=10
            }else if(tp == type.paper){
                info = "KALAH" // lose
            }else{
                info = "DRAW" // equal
            }
        }

        tv_score.text = "Score : $score"
        tv_info.text = "$info"


        tv_info.postDelayed({
            tv_info.text = ""
            ViewCompat.animate(img_enemy).translationY(-400f).duration = 1000
            ViewCompat.animate(img_mc).translationY(600f).duration = 800
            tv_info.postDelayed({
                randomEnemy()
            }, 1000)

        }, 1000)
    }

    fun randomEnemy(){

        when (random(0, 3)){
            2-> {
                curType = type.rock
                img_enemy.setImageResource(R.drawable.ic_rock)
            }
            1-> {
                curType = type.paper
                img_enemy.setImageResource(R.drawable.ic_paper)
            }
            0-> {
                curType = type.scissor
                img_enemy.setImageResource(R.drawable.ic_scissor)
            }

        }

        ViewCompat.animate(img_enemy).translationY(400f).duration = 100


    }

    fun random(from: Int, to: Int) : Int {
        return Random().nextInt(to - from) + from
    }

    fun restart(){
        score = 0
        time = 20
        curType = null
        curState = null

        gameEngine.init(this)
        gameEngine.start()
    }
}